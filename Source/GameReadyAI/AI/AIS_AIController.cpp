// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BehaviorTree.h"
#include "AIS_AIController.h"

AAIS_AIController::AAIS_AIController(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer)
{
	static auto BTFinder{ ConstructorHelpers::FObjectFinder<UBehaviorTree>(TEXT("BehaviorTree'/Game/AIStream/AI/AIS_BT.AIS_BT'")) };
	if (BTFinder.Succeeded())
	{
		BehaviorTree = BTFinder.Object;
	}
}

void AAIS_AIController::BeginPlay()
{
	Super::BeginPlay();
	check(BehaviorTree && "No behavior tree for AIS AIController.");
	RunBehaviorTree(BehaviorTree);
}
