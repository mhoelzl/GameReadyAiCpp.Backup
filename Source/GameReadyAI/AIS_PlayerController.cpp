// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "Components/ArrowComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "AIS_PlayerPawn.h"
#include "AIS_PlayerController.h"

AAIS_PlayerController::AAIS_PlayerController(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer),
	MaxClickTime{ 0.18 },
	bLeftMouseButtonHeld{ false },
	bRightMouseButtonHeld{ false },
	MovementRate{ 10.0f },
	RotationRate{ 10.0f }
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
	bEnableMouseOverEvents = true;
	bEnableTouchOverEvents = true;
}

void AAIS_PlayerController::Tick(float DeltaSeconds)
{
	if (bLeftMouseButtonHeld)
	{
		OnLeftMouseButtonHeld();
	}
	if (bRightMouseButtonHeld)
	{
		OnRightMouseButtonHeld();
	}
}

void AAIS_PlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveForward", this, &AAIS_PlayerController::MoveForward);
	InputComponent->BindAxis("MoveRight");

	InputComponent->BindKey(EKeys::LeftMouseButton, IE_Pressed, this, &AAIS_PlayerController::OnLeftMouseButtonPressed);
	InputComponent->BindKey(EKeys::LeftMouseButton, IE_Released, this, &AAIS_PlayerController::OnLeftMouseButtonReleased);
	InputComponent->BindKey(EKeys::RightMouseButton, IE_Pressed, this, &AAIS_PlayerController::OnRightMouseButtonPressed);
	InputComponent->BindKey(EKeys::RightMouseButton, IE_Released, this, &AAIS_PlayerController::OnRightMouseButtonReleased);
}

void AAIS_PlayerController::OnLeftMouseButtonClicked()
{
	UE_LOG(LogTemp, Log, TEXT("*** Left Mouse Button Clicked ***"));
}

void AAIS_PlayerController::OnLeftMouseButtonHeld()
{
	UE_LOG(LogTemp, Log, TEXT("*** Left Mouse Button Held ***"));

}

void AAIS_PlayerController::OnRightMouseButtonClicked()
{
	UE_LOG(LogTemp, Log, TEXT("*** Right Mouse Button Clicked ***"));
}

void AAIS_PlayerController::OnRightMouseButtonHeld()
{
	// UE_LOG(LogTemp, Log, TEXT("*** Right Mouse Button Held ***"));

	float ControlRotationYaw = GetControlRotation().Yaw;
	float MouseX, MouseY;
	GetInputMouseDelta(MouseX, MouseY);
	FRotator NewControlRotation(0.0f, ControlRotationYaw + MouseX * RotationRate, 0.0f);
	SetControlRotation(NewControlRotation);

	FRotator SpringArmRotation{ StoredPawn->SpringArm->GetComponentRotation() };
	SpringArmRotation.Yaw = NewControlRotation.Yaw;
	SpringArmRotation.Pitch += MouseY * RotationRate;
	StoredPawn->SpringArm->SetWorldRotation(SpringArmRotation);
}

void AAIS_PlayerController::MoveForward(float Value)
{
	if (StoredPawn && StoredPawn->IsActorInitialized() && !StoredPawn->IsPendingKill())
	{
		FVector ForwardVector{ UKismetMathLibrary::GetForwardVector(GetControlRotation()) };
		FVector ForwardInput{ Value * ForwardVector };

		FVector RightVector{ UKismetMathLibrary::GetRightVector(GetControlRotation()) };
		FVector RightInput{ InputComponent->GetAxisValue(FName(TEXT("MoveRight"))) * RightVector };

		FVector MovementInput{ MovementRate * (ForwardInput + RightInput) };
		MovementInput.Z = 0.0f;

		StoredPawn->AddActorLocalOffset(MovementInput);

		FRotator ControlRotation = GetControlRotation();
		StoredPawn->ControllerArrow->SetWorldRotation(ControlRotation);
	}
}

void AAIS_PlayerController::DetectLeftMouseButtonHeld()
{
	bLeftMouseButtonHeld = true;
	OnLeftMouseButtonHeld();
}

void AAIS_PlayerController::OnLeftMouseButtonPressed()
{
	FTimerManager& TimerManager{ GetWorld()->GetTimerManager() };
	TimerManager.SetTimer(LeftMouseButtonTimer, this, &AAIS_PlayerController::DetectLeftMouseButtonHeld, MaxClickTime, false);
}

void AAIS_PlayerController::OnLeftMouseButtonReleased()
{
	bLeftMouseButtonHeld = false;

	FTimerManager& TimerManager{ GetWorld()->GetTimerManager() };
	if (TimerManager.IsTimerActive(LeftMouseButtonTimer))
	{
		TimerManager.ClearTimer(LeftMouseButtonTimer);
		OnLeftMouseButtonClicked();
	}
}

void AAIS_PlayerController::DetectRightMouseButtonHeld()
{
	bRightMouseButtonHeld = true;
	OnRightMouseButtonHeld();
}

void AAIS_PlayerController::OnRightMouseButtonPressed()
{
	FTimerManager& TimerManager{ GetWorld()->GetTimerManager() };
	TimerManager.SetTimer(RightMouseButtonTimer, this, &AAIS_PlayerController::DetectRightMouseButtonHeld, MaxClickTime, false);
}

void AAIS_PlayerController::OnRightMouseButtonReleased()
{
	bRightMouseButtonHeld = false;

	FTimerManager& TimerManager{ GetWorld()->GetTimerManager() };
	if (TimerManager.IsTimerActive(RightMouseButtonTimer))
	{
		TimerManager.ClearTimer(RightMouseButtonTimer);
		OnRightMouseButtonClicked();
	}
}
