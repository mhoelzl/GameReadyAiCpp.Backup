// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BTTask_ChooseRandomLocation.generated.h"

class UBehaviorTree;

/**
 * Pick a random location on the navmesh within a certain max distance of the player.
 */
UCLASS()
class GAMEREADYAI_API UBTTask_ChooseRandomLocation : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UBTTask_ChooseRandomLocation(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	virtual void InitializeFromAsset(UBehaviorTree& Asset) override;

	UPROPERTY(EditAnywhere, Category = "Movement")
	struct FBlackboardKeySelector MoveToKey;

	UPROPERTY(EditAnywhere, Category = "Movement", Meta = (ClampMin = 0.0, UIMin = 0.0))
	float MaxRadius;
	
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual FString GetStaticDescription() const override;
};
