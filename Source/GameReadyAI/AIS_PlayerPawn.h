// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "GameFramework/Pawn.h"
#include "AIS_PlayerPawn.generated.h"

UCLASS()
class GAMEREADYAI_API AAIS_PlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AAIS_PlayerPawn(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Components")
	USceneComponent* DefaultRootComponent;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Camera")
	USpringArmComponent* SpringArm;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Camera")
	UCameraComponent* Camera;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Visualization")
	UArrowComponent* ControllerArrow;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Visualization")
	UArrowComponent* PawnArrow;

protected:
	virtual void PossessedBy(AController* NewController) override;

	class AAIS_PlayerController* StoredController;
};
