// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "GameFramework/PlayerController.h"
#include "AIS_PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class GAMEREADYAI_API AAIS_PlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AAIS_PlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	virtual void Tick(float DeltaSeconds) override;

	class AAIS_PlayerPawn* StoredPawn;
	virtual void SetupInputComponent() override;

protected:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "UI")
	float MaxClickTime;

	bool bLeftMouseButtonHeld;
	virtual void OnLeftMouseButtonClicked();
	virtual void OnLeftMouseButtonHeld();

	bool bRightMouseButtonHeld;
	virtual void OnRightMouseButtonClicked();
	virtual void OnRightMouseButtonHeld();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Movement")
	float MovementRate;
	float RotationRate;

	void MoveForward(float Value);

private:
	FTimerHandle LeftMouseButtonTimer;
	void DetectLeftMouseButtonHeld();
	void OnLeftMouseButtonPressed();
	void OnLeftMouseButtonReleased();

	FTimerHandle RightMouseButtonTimer;
	void DetectRightMouseButtonHeld();
	void OnRightMouseButtonPressed();
	void OnRightMouseButtonReleased();
};
