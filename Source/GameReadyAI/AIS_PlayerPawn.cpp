// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "Components/ArrowComponent.h"
#include "AIS_PlayerController.h"
#include "AIS_PlayerPawn.h"


// Sets default values
AAIS_PlayerPawn::AAIS_PlayerPawn(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	DefaultRootComponent = CreateDefaultSubobject<USceneComponent>("DefaultRootComponent");
	SetRootComponent(DefaultRootComponent);

	SpringArm = CreateDefaultSubobject<USpringArmComponent>("SprintArm");
	SpringArm->AttachTo(DefaultRootComponent);
	SpringArm->SetRelativeLocation(FVector(0.0f, 0.0f, 100.0f));
	SpringArm->SetRelativeRotation(FRotator(330.0f, 0.0f, 0.0f));

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	Camera->AttachTo(SpringArm);

	ControllerArrow = CreateDefaultSubobject<UArrowComponent>("ControllerArrow");
	ControllerArrow->AttachTo(DefaultRootComponent);
	ControllerArrow->SetRelativeLocation(FVector(0.0f, 0.0f, 30.0f));
	ControllerArrow->bHiddenInGame = false;
	ControllerArrow->SetArrowColor_New(FLinearColor::Blue);
	ControllerArrow->ArrowSize = 0.8f;

	PawnArrow = CreateDefaultSubobject<UArrowComponent>("PawnArrow");
	PawnArrow->AttachTo(DefaultRootComponent);
	PawnArrow->SetRelativeLocation(FVector(0.0f, 0.0f, 10.0f));
	PawnArrow->bHiddenInGame = false;
}

void AAIS_PlayerPawn::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	AAIS_PlayerController* PlayerController = Cast<AAIS_PlayerController>(NewController);

	if (PlayerController)
	{
		StoredController = PlayerController;
		PlayerController->StoredPawn = this;
	}
}

