// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "GameFramework/Character.h"
#include "AIS_Character.generated.h"

UCLASS()
class GAMEREADYAI_API AAIS_Character : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAIS_Character(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	// The maximum health of the character
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health", Meta = (ClampMin = 0.0, UIMin = 0.0))
	float MaxHealth;

	// The health of the character, between 0 (dead) to Max Health.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health", Meta = (ClampMin = 0.0, UIMin = 0.0))
	float Health;

	// Add a certain amount of health
	UFUNCTION(BlueprintCallable, Category = "Health")
	void AddHealth(float Amount);

	// The maximum stamina of the character
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health", Meta = (ClampMin = 0.0, UIMin = 0.0))
	float MaxStamina;

	// The stamina of the character, between 0 (dead) to 100 (max stamina).
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health",
		Meta = (ClampMin = 0.0, UIMin = 0.0, ClampMax = 100.0, UIMax = 100.0))
	float Stamina;

	// Add a certain amount of health
	UFUNCTION(BlueprintCallable, Category = "Health")
	void AddStamina(float Amount);

	// The rate with which the stamina loss of the character is ticked.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health", Meta = (ClampMin = 0.0, UIMin = 0.0))
	float StaminaTickTime; // Called StamTickRate in the video

	// The we apply to stamina for each tick.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health", Meta = (ClampMin = 0.0, UIMin = 0.0))
	float StaminaDecayRate;

	// The rate with which the character heals itself
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health", Meta = (ClampMin = 0.0, UIMin = 0.0))
	float HealRate;

	// The health of the character, between 0 (dead) to 100 (max health).
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health",
		Meta = (ClampMin = 0.0, UIMin = 0.0, ClampMax = 100.0, UIMax = 100.0))
	float HealthLossOnZeroStamina;

	FTimerHandle StaminaTickTimer;

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, 
	AActor* DamageCauser) override;
	
protected:
	void TickDownStamina();
};
