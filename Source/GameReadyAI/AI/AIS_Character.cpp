// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "AI/AIS_AIController.h"
#include "AIS_Character.h"


// Sets default values
AAIS_Character::AAIS_Character(const FObjectInitializer& ObjectInitializer) :
	Super(ObjectInitializer),
	MaxHealth{ 100.0f },
	Health{ MaxHealth },
	MaxStamina{ 100.0f },
	Stamina{ MaxStamina },
	StaminaTickTime{ 0.5f },
	StaminaDecayRate{ 20.0f },
	HealRate{ 5.0f },
	HealthLossOnZeroStamina{ 10.0f }
{
	PrimaryActorTick.bCanEverTick = true;

	GetCapsuleComponent()->bGenerateOverlapEvents = true;

	static auto SkeletalMeshFinder{ ConstructorHelpers::FObjectFinder<USkeletalMesh>(TEXT("SkeletalMesh'/Game/Mannequin/Character/Mesh/SK_Mannequin.SK_Mannequin'")) };
	if (SkeletalMeshFinder.Succeeded())
	{
		USkeletalMeshComponent* Mesh = GetMesh();
		Mesh->SetSkeletalMesh(SkeletalMeshFinder.Object);
		Mesh->SetRelativeLocation(FVector(0.0f, 0.0f, -85.0f));
		Mesh->SetRelativeRotation(FRotator(0.0f, 270.0f, 0.0f));

		static auto AnimBPFinder{ ConstructorHelpers::FObjectFinder<UAnimBlueprintGeneratedClass>(TEXT("AnimBlueprint'/Game/Mannequin/Animations/ThirdPerson_AnimBP.ThirdPerson_AnimBP_C'")) };
		if (AnimBPFinder.Succeeded())
		{
			Mesh->SetAnimInstanceClass(AnimBPFinder.Object);
		}
	}


	auto* MovementComponent{ GetCharacterMovement() };
	MovementComponent->MaxWalkSpeed = 400.0f;
	MovementComponent->MaxWalkSpeedCrouched = 200.0f;
	MovementComponent->MaxCustomMovementSpeed = 400.0f;
	MovementComponent->MaxAcceleration = 600.0f;

	bUseControllerRotationYaw = false;
	MovementComponent->RotationRate.Yaw = 240.0f;
	MovementComponent->bOrientRotationToMovement = true;
	MovementComponent->bUseControllerDesiredRotation = false;

	AIControllerClass = AAIS_AIController::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	AutoPossessPlayer = EAutoReceiveInput::Disabled;
}

// Called when the game starts or when spawned
void AAIS_Character::BeginPlay()
{
	Super::BeginPlay();

	auto StaminaDelegate = FTimerDelegate::CreateUObject(this, &AAIS_Character::TickDownStamina);

	GetWorld()->GetTimerManager().SetTimer(StaminaTickTimer, StaminaDelegate, StaminaTickTime, true);
}

// Called every frame
void AAIS_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AAIS_Character::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
}

void AAIS_Character::AddHealth(float Amount)
{
	Health = FMath::Clamp(Health + Amount, -1.0f, MaxHealth);
}

void AAIS_Character::AddStamina(float Amount)
{
	Stamina = FMath::Clamp(Stamina + Amount, -1.0f, MaxStamina);
}

float AAIS_Character::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	UE_LOG(LogTemp, Log, TEXT("*** OW ***"));
	Health -= Damage;
	if (Health < 0.0f)
	{
		GetController()->UnPossess();
		Destroy();
		Health = 0.0f;
	}
	return Health;
}

void AAIS_Character::TickDownStamina()
{
	Stamina -= StaminaDecayRate * StaminaTickTime;

	if (Stamina <= 0.0f)
	{
		Stamina = 0.0f;
		struct FDamageEvent DamageEvent;
		TakeDamage(HealthLossOnZeroStamina, DamageEvent, GetController(), this);
	}
	else
	{
		AddHealth(HealRate * StaminaTickTime);
	}

	UE_LOG(LogTemp, Log, TEXT("Stamina: %.0f, Health: %.0f"), Stamina, Health);
}

