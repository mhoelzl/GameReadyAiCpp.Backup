// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "BaseFood.h"
#include "AI/AIS_Character.h"


// Sets default values
ABaseFood::ABaseFood() :
	StaminaRestore{ 50.0f }
{
	PrimaryActorTick.bCanEverTick = false;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	if (RootComponent)
	{
		StaticMesh->AttachTo(RootComponent);
	}
	else
	{
		SetRootComponent(StaticMesh);
	}

	static auto Barrel = TEXT("StaticMesh'/Game/InfinityBladeGrassLands/Environments/Misc/Exo_Deco01/StaticMesh/SM_WoodenBarrel_Intact.SM_WoodenBarrel_Intact'");
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshFinder(Barrel);
	if (MeshFinder.Succeeded())
	{
		StaticMesh->StaticMesh = MeshFinder.Object;
	}

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	CollisionSphere->AttachTo(RootComponent);
	CollisionSphere->SetRelativeLocation(FVector(0.0f, 0.0f, 100.0f));
	CollisionSphere->SetRelativeScale3D(FVector(3.5f));

	CollisionSphere->bGenerateOverlapEvents = true;
	CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &ABaseFood::HandleOverlap);
}

// Called when the game starts or when spawned
void ABaseFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseFood::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ABaseFood::HandleOverlap(AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	UE_LOG(LogTemp, Log, TEXT("*** BaseFood Overlap ***"));
	AAIS_Character* Character = Cast<AAIS_Character>(OtherActor);
	Character->AddStamina(StaminaRestore);
}

