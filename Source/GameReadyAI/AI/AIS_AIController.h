// Copyright 2015 Matthias Hölzl, All Rights Reserved.

#pragma once

#include "AIController.h"
#include "AIS_AIController.generated.h"

UCLASS()
class GAMEREADYAI_API AAIS_AIController : public AAIController
{
	GENERATED_BODY()

public:
	AAIS_AIController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "AI")
	class UBehaviorTree* BehaviorTree;

};
