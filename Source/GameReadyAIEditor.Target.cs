// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class GameReadyAIEditorTarget : TargetRules
{
	public GameReadyAIEditorTarget(TargetInfo Target)
	{
		Type = TargetType.Editor;
	}

	//
	// TargetRules interface.
	//

	public override void SetupBinaries(
		TargetInfo Target,
		ref List<UEBuildBinaryConfiguration> OutBuildBinaryConfigurations,
		ref List<string> OutExtraModuleNames
		)
	{
		OutExtraModuleNames.Add("GameReadyAI");
	}


    public override void GetModulesToPrecompile(TargetInfo Target, List<string> ModuleNames)
    {
        /*
        ModuleNames.Add("Core");
        ModuleNames.Add("CoreUObject");
        ModuleNames.Add("Engine");
        ModuleNames.Add("InputCore");
        ModuleNames.Add("SlateCore");
        ModuleNames.Add("Slate");
        */
    }
}
