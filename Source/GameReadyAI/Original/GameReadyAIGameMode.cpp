// Copyright 2015 Matthias H�lzl, All Rights Reserved.

#include "GameReadyAI.h"
#include "AIS_PlayerPawn.h"
#include "AIS_PlayerController.h"
#include "GameReadyAIGameMode.h"
#include "GameReadyAICharacter.h"

AGameReadyAIGameMode::AGameReadyAIGameMode()
{

	DefaultPawnClass = AAIS_PlayerPawn::StaticClass();
	PlayerControllerClass = AAIS_PlayerController::StaticClass();
}
